#!/bin/bash

javaVersion=$(java -version 2>&1 | head -n 1 | cut -d "\"" -f 2)
# Check if good Java version before everything
if [[ ${javaVersion:2} == 17 ]]; then
  echo "You should use Java 17 to run GAMA"
  echo "Found you using version : $javaVersion"
  exit 1
fi

memory="0"

for arg do
  shift
  case $arg in
    -m) 
    memory="${1}" 
    shift 
    ;;
    *) 
    set -- "$@" "$arg" 
    ;;
  esac
done

if [[ $memory == "0" ]]; then
  memory=$(grep Xmx "$( dirname $( realpath "${BASH_SOURCE[0]}" ) )"/../Gama.ini || echo "-Xmx4096m")
else
  memory=-Xmx$memory
fi

workspaceCreate=0
case "$@" in 
  *-help*|*-version*|*-validate*|*-test*|*-xml*|*-batch*|*-write-xmi*|*-socket*)
    workspaceCreate=1
    ;;
esac

echo "******************************************************************"

#MY_JAVA_DIR=${HOME}/_java
MY_JAVA_DIR=${HOME}
MY_JAVA_SYSTEM_PREFS_DIR=${MY_JAVA_DIR}/.systemPrefs
MY_JAVA_USER_PREFS_DIR=${MY_JAVA_DIR}/.userPrefs
mkdir -p ${MY_JAVA_DIR}
mkdir -p ${MY_JAVA_SYSTEM_PREFS_DIR}
mkdir -p ${MY_JAVA_USER_PREFS_DIR}
chmod -R 777 ${MY_JAVA_DIR}

MY_TEMP_DIR=${HOME}/_temp_dir
mkdir -p ${MY_TEMP_DIR}

MY_CONFIGURATION_DIR=${HOME}/configuration
mkdir -p ${MY_CONFIGURATION_DIR}

MY_DATA_DIR=${HOME}
mkdir -p ${MY_DATA_DIR}

chmod -R 777 ${HOME}

ls -all ${HOME}

java -XshowSettings:properties

echo "******************************************************************"
echo "* GAMA version 1.9.1                                             *"
echo "* http://gama-platform.org                                       *"
echo "* (c) 2007-2023 UMI 209 UMMISCO IRD/SU & Partners                *"
echo "******************************************************************"
passWork=.workspace
# w/ output folder
if [ $workspaceCreate -eq 0 ]; then
  # create output folder if not existing
  if [ ! -d "${@: -1}" ]; then
      mkdir ${@: -1}
  fi
  # create workspace in output folder
  passWork=${@: -1}/.workspace$(find ${@: -1} -name ".workspace*" | wc -l)
  mkdir -p $passWork

# w/o output folder
else
  # create workspace in current folder
  #passWork=.workspace$(find ./ -maxdepth 1 -name ".workspace*" | wc -l)
  passWork=${MY_DATA_DIR}/.workspace$(find ./ -maxdepth 1 -name ".workspace*" | wc -l)
fi

echo ""
echo "AAA Before java -cp"
echo ""
echo "passWork $passWork"
echo ""

#if ! java -cp "$( dirname $( realpath "${BASH_SOURCE[0]}" ) )"/../plugins/org.eclipse.equinox.launcher*.jar -Xms512m $memory -Djava.awt.headless=true org.eclipse.core.launcher.Main -configuration "$( dirname $( realpath "${BASH_SOURCE[0]}" ) )"/configuration -application msi.gama.headless.product -data $passWork "$@"; then

chmod -R 777 ${HOME}
ls -all ${HOME}

if ! java -cp "$( dirname $( realpath "${BASH_SOURCE[0]}" ) )"/../plugins/org.eclipse.equinox.launcher*.jar -Xms512m $memory -Djava.io.tmpdir=$MY_TEMP_DIR -Djava.util.prefs.systemRoot=${MY_JAVA_SYSTEM_PREFS_DIR} -Djava.util.prefs.userRoot=${MY_JAVA_USER_PREFS_DIR} -Djava.awt.headless=true org.eclipse.core.launcher.Main -configuration $MY_CONFIGURATION_DIR -application msi.gama.headless.product -data $passWork "$@"; then
    echo "Error in you command, here's the log :"
    cat $passWork/.metadata/.log
    exit 1
fi

echo ""
echo "AAA After java -cp"
echo ""

# Note 
#   cat $passWork/.metadata/.log
# replaced by 
#   cat ${MY_DATA_DIR}/.metadata/.log
#

echo "ps -T"
ps -T
echo "****"

#echo ""
#echo "exit 0"
#echo ""
#exit 0
