# CI - Integration Continue

Un exemple
==========

  Build-Install
  -------------

  .gitlab-ci.yml : verifies docker/Dockerfile file and build images

  => Container Registry :

  registry.forgemia.inra.fr/nathalie.rousse/sandbox:ec78e479d5ae86eeda8b9d2783de01f26a4c9dc9

  registry.forgemia.inra.fr/nathalie.rousse/sandbox:latest

  Use
  ---

  - Prerequisite (sandbox being private) : generate a token (read_registry,
    write_registry) to be able to login

  - Then (private project) :

    docker login registry.forgemia.inra.fr

    docker pull registry.forgemia.inra.fr/nathalie.rousse/sandbox:ec78e4...

