# Container protonr

## Desc

Container dedié au prototype d'outil 'protonr', préparé pour livraison a siwaa.

Le code du tool 'protonr' se trouve sous : branch 'protonr' of 'use' project.

## Build

.gitlab-ci.yml : build image protonr.simg

=> Container Registry :
   oras://registry.forgemia.inra.fr/nathalie.rousse/sandbox/protonr:latest

## Use

*(temporary 'public' status project pour abstraction des droits d'acces)*

```bash
# protonr.simg
singularity pull protonr.simg oras://registry.forgemia.inra.fr/nathalie.rousse/sandbox/protonr:latest

# singularity exec

singularity exec protonr.simg python3 /WS/protonr/erecord/_f_main.py

singularity exec protonr.simg python3 /WS/protonr/erecord/_f_main.py '{"key1":"value1","key3":34.8}'

singularity exec protonr.simg /bin/bash -c "python3 /WS/protonr/erecord/_f_main.py '{\"key1\":\"value1\",\"key3\":34.8}'"

#python3 _f_main.py '{"key1":"value1","key2":"value2","key3":34.8}'
#
#DATA='{"key1":"value1","key2":"value2","key3":34.8}'
#python3 _f_main.py $DATA
#EXECMD="python3 _f_main.py $DATA"
#$EXECMD

```
